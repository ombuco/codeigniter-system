# CodeIgniter System
The purpose is to keep CodeIgniter's system files is a shared place for all our projects at (http://ombu.co)["Ombu"], private and public. We don't expect to have anybody use this. We keep CodeIgniter updated and we change how the Controller loads.

### Setup
To add this as a GIT Submodule, enter this command:

~~~~~
git submodule add git@bitbucket.org/ombuco/codeigniter-system.git system-ci
git submodule update --init
~~~~~

CodeIgniter's system file will be in the `system-ci` folder.

### Author
This is made by David K. - <http://ombu.co/>.